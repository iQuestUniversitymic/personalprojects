import React from 'react';
import { StyleSheet, Text, View, FlatList, TextInput, TouchableOpacity, AsyncStorage } from 'react-native';
import OfflineNotification from './components/OfflineNotification';

export default class App extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      orderItems: [],
      menuItems: [],
      lastMenuItems: [],
      quantity: '',
      code: '',
      name: ''  
    };
    
    this.timeout = 0;
    this.apiURI = 'http://192.168.0.157:3000/';

  }

  async componentDidMount(){
    let items = await this.getOrderItemsFromMemory();
    
    if(items){
      let itemsJSON = JSON.parse(items);
      this.setState({orderItems : [...itemsJSON]});
    }
  
  }

  getOrderItemsFromMemory = async () => {
    return await AsyncStorage.getItem('OrderItems');
  }

  search = (event) => {
    var searchText = event.nativeEvent.text; // this is the search text
    
    if(this.timeout){
      clearTimeout(this.timeout);
    } 
    
    this.timeout = setTimeout( () => {
    
      fetch(this.apiURI + 'MenuItem?q=' + searchText)
      .then(response => {
        
        let items = JSON.parse(response._bodyText);
        let menuItems = items.slice(0,5);
        this.setState({ menuItems : menuItems});
        this.setState({lastMenuItems: menuItems});
      })
      .catch(() => {
        this.setState({menuItems : lastMenuItems});        
      });

    }, 2000);
  }

  render() {
    return (
      <View style={styles.container}>

        <OfflineNotification/>

        <TextInput style={styles.textInput} placeholder='Search item...'
                    onChange={(event) => this.search(event) }
                    underlineColorAndroid='transparent' />      

        <TextInput style={styles.textInput2} placeholder='Quantity'
                    onChange={(event) => this.setState({quantity : event.nativeEvent.text})}
                    underlineColorAndroid='transparent' />      
        
        <TouchableOpacity style={styles.btn} onPress={() => {
          
          if(this.state.quantity &&
              this.state.code &&
              this.state.name){
                
                let newObj = {
                  code : this.state.code,
                  name : this.state.name
                } 
                
                let objToPost = {
                  table : 1,
                  code : this.state.code,
                  quantity : this.state.quantity,
                  free : false
                }

                fetch(this.apiURI + "OrderItem",{
                  method: 'POST',
                  headers: {
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify(objToPost)
                })
                .then(response => response.json())
                .catch(() => console.log("error at posting order"));

                let newOrderItems = [...this.state.orderItems, newObj];
                this.setState({orderItems : [...newOrderItems], code:'', name:'', quantity:''})

                let newOrderItemsAsync = JSON.stringify(newOrderItems);
                AsyncStorage.setItem('OrderItems', newOrderItemsAsync);

              }
        }}>
                            <Text>Order</Text>
        </TouchableOpacity>

        <FlatList 
          data = {this.state.menuItems}
          showsVerticalScrollIndicator={true}
            renderItem={({item}) =>

            <TouchableOpacity style={styles.flatview} onPress={ () => {this.setState({code : item.code, name: item.name})}}>
              <Text style={styles.name}>{item.name}</Text>
            </TouchableOpacity>

            }
          keyExtractor={item => item.code.toString()}
          style={styles.orderList}
          />

          <View style={styles.separator}>
            <Text></Text>
          </View>

          <FlatList 
            data = {this.state.orderItems}
            showsVerticalScrollIndicator={true}
            renderItem={({item}) =>

            <View style={styles.flatview}>
              <Text style={styles.name}>{item.name}</Text>
            </View>

            }
            keyExtractor={item => item.code.toString()}
            style={styles.orderList}
          />

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    alignSelf: 'stretch',
    marginBottom: 70
  },
  flatview: {
    justifyContent: 'center',
    paddingTop: 10,
    borderRadius: 2,
    paddingBottom: 10,
    borderWidth: 4,
    borderColor : 'white',
    alignItems: 'center'
  },
  name: {
    fontSize: 18
  },
  email: {
    color: 'red'
  },
  orderList: {
    flex: .3,
    top: 10,
    marginLeft : 10,
    marginRight : 10,
    backgroundColor: 'lightgray'
  },
  separator: {
    flex: .2,
    marginLeft : 10,
    marginRight : 10,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textInput: {
    marginTop: 60,
    alignSelf: 'stretch',
    padding: 5,
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth : 2,
    borderColor : 'lightgray'
},
  textInput2: {
    marginTop: 5,
    alignSelf: 'stretch',
    padding: 5,
    margin: 10,
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth : 2,
    borderColor : 'lightgray'
},
  btn: {
    alignSelf: 'stretch',
    backgroundColor: '#bebebe',
    padding: 20,
    alignItems: 'center',
    color: 'white',
    borderRadius: 40,
    marginTop: 5,
    margin: 10
},
});
