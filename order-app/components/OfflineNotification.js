import React, { PureComponent } from 'react';
import { View, Text, NetInfo, Dimensions, StyleSheet } from 'react-native';
const { width } = Dimensions.get('window');


const MiniOfflineSign = () => {
  return (
    <View style={styles.offlineContainer}>
      <Text style={styles.text}>No Internet Connection</Text>
    </View>
  );
}

const MiniOnlineSign = () => {
    return (
    <View style={styles.onlineContainer}>
      <Text style={styles.text}>You're Online</Text>
    </View>
    );
}

export default class OfflineNotification extends PureComponent {
  
    state = {
        isConnected : true
    }

    componentDidMount(){
        NetInfo.isConnected.addEventListener('connectionChange', this.handleConnectivityChange);
    }

    componentWillUnmount(){
        NetInfo.isConnected.removeEventListener('connectionChange', this.handleConnectivityChange);
    }

    handleConnectivityChange = isConnected => {
        if (isConnected) {
            this.setState({isConnected});
        } else {
            this.setState({isConnected});
        }
    }

    render() {
        
        if(!this.state.isConnected){
            return <MiniOfflineSign />;
        }
        return <MiniOnlineSign />;
  }
}

const styles = StyleSheet.create({
  offlineContainer: {
    backgroundColor: '#b52424',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    position: 'absolute',
    top: 24
  },
  text: { 
    color: '#fff',
  },
  onlineContainer: {
    backgroundColor: '#76ff03',
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    width,
    position: 'absolute',
    top: 24
  },
});