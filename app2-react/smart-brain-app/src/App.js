import React, { Component } from 'react';
import './App.css';

import Navigation from './/components/Navigation/Navigation';
import SignIn from './/components/SignIn/SignIn';
import Register from './/components/Register/Register';
import Logo from './/components/Logo/Logo';
import ImageLinkForm from './/components/ImageLinkForm/ImageLinkForm';
import Rank from './/components/Rank/Rank';
import FaceRecognition from './/components/FaceRecognition/FaceRecognition';
import Particles from 'react-particles-js';

import { particlesParams } from './constants';

const initialState = { 
  input: '',
  imageUrl: '',
  box: {},
  route: 'signIn',
  user: {
    id : '',
    name: '',
    email: '',
    password: '',
    entries: 0,
    joined: ''
  }  
}
class App extends Component {

  constructor(){
    super();
    this.state = initialState;
  }

  loadUser = (data) => {

    const {id, name, email, password, entries, joined} = data;

    this.setState({
      user: {
        id : id,
        name: name,
        email: email,
        password: password,
        entries: entries,
        joined: joined
      } 
    });

  }

  calculateFaceLocation = (regions) => {
    const faceCoordinates = regions[0].region_info.bounding_box;
    const image = document.getElementById('inputImage');
    const width = Number(image.width);
    const height = Number(image.height);

    const {top_row, left_col, bottom_row, right_col} = faceCoordinates; 

    const leftBound = left_col * width;
    const topBound = top_row * height;
    const rightBound = width - (right_col * width);
    const bottomBound = height - (bottom_row * height);  

    const box = {
      leftBound: leftBound, 
      rightBound: rightBound,
      topBound: topBound,
      bottomBound: bottomBound     
    };

    return box;
  }

  displayBoundaryBox = (box) => {
    this.setState({box: box});
  }

  onInputChange = (event) => {
    this.setState({input : event.target.value});
  }

  onPictureSubmit = () => {
    this.setState({imageUrl : this.state.input});
    fetch('https://young-ridge-32411.herokuapp.com/imageurl', {
            method: 'post',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
              input: this.state.input
            })
          })
      .then(response => response.json())
      .then(response => {    
        if (response) {
          fetch('https://young-ridge-32411.herokuapp.com/image', {
            method: 'put',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
              id: this.state.user.id
            })
          })
            .then(response => response.json())
            .then(entries => 
              this.setState(Object.assign(this.state.user, {entries: entries})));
              
          this.displayBoundaryBox(this.calculateFaceLocation(response.outputs[0].data.regions));
        }})
      .catch(error => console.log(error));
  }

  onRouteChange = (route) => {

    if (route === 'signint' || route === 'register') {
      this.setState(initialState);
    }

    this.setState({route : route});
  } 

  render() {
    return (
      <div className="App">
        <Particles
          params={particlesParams} className='particles'/>
        
        { this.state.route === 'home' ? 
          <div>
            <Navigation onRouteChange={this.onRouteChange}/>
            <Logo />
            <Rank name={this.state.user.name} entries={this.state.user.entries}/>
            <ImageLinkForm 
              onInputChange={this.onInputChange} 
              onButtonSubmit={this.onPictureSubmit}
            />
            <FaceRecognition box={this.state.box} imageUrl={this.state.imageUrl}/>
          </div> :
            ( this.state.route === 'signIn' ? 
              <SignIn onRouteChange={this.onRouteChange} loadUser={this.loadUser}/> : 
              <Register onRouteChange={this.onRouteChange} loadUser={this.loadUser}/>
            )
        }
      </div>
    );
  }
}

export default App;
