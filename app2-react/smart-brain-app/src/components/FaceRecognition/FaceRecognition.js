import React from 'react';
import './FaceRecognition.css';

const FaceRecognition = ({imageUrl, box}) => {


    return(
        <div className='center ma'>
            <div className='absolute mt2'>
                <img id='inputImage' src={imageUrl} alt='Face recognition with bounding boxes' width='500px' height='auto'/>
                <div className='bounding-box' style={{top:box.topBound, right:box.rightBound, bottom:box.bottomBound, left:box.leftBound}}></div>
            </div>
        </div>
    );


}


export default FaceRecognition;