const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const bcrypt = require('bcrypt-nodejs');

const { handleRegister } = require('./controllers/register');
const { handleSignIn } = require('./controllers/signIn');
const { handleProfile } = require('./controllers/profile');
const { handleEntries, handleApiCall } = require('./controllers/entries');

const { db } = require('./repository/dbConfig');

const app = express();
app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => {res.send('it is working!')} );

app.post('/signin', async (req, res) => {handleSignIn(req, res, db, bcrypt)} );

app.post('/register', async (req, res) => {handleRegister(req, res, db, bcrypt)});

app.get('/profile/:id', async (req, res) => {handleProfile(req, res, db)});

app.put('/image', async (req, res) => {handleEntries(req, res, db)});

app.post('/imageurl', async (req, res) => {handleApiCall(req, res)});

app.listen(process.env.PORT || 3000, () => {
    console.log(`app is running on port ${process.env.PORT}`);
})