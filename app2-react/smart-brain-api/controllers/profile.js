const findProfileById = (id, db) =>{

    return profile = db('users')
                .returning('*')
                .select('*')
                .where('id', id)
                .then(user => {
                    return user[0];
                });
}   

const profileHandler = async (req, res, db) => {
    const {id} = req.params;
    
    const profile = await findProfileById(id, db);
    if (profile) {
        return res.json(profile);
    }
    res.status(404).json("User doesn't exist!");
}

module.exports = {
    profileHandler : profileHandler
}