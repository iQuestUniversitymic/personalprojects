const Clarifai = require('clarifai');


const app = new Clarifai.App({
    apiKey: '26d057893c2142dd992026cd33a2d8d5'
   });
  
const handleApiCall = (req, res) => {
    app.models
    .predict(
        Clarifai.FACE_DETECT_MODEL, 
        req.body.input)
    .then(data => {
        res.json(data);
    })
    .catch(error => res.status(400).json('unable to work with api'));
}


const updateNrEntries = (id, db) =>{
  
    return updatedEntries = db('users')
                            .where('id', id)
                            .increment('entries', 1)
                            .returning('entries')
                            .then( entries => {
                                return entries[0];
                            })

}

const handleEntries = async (req, res, db) => {
    const {id} = req.body;

    const updated = await updateNrEntries(id, db);
    if(updated){
        return res.json(updated);
    }
    res.status(404).json("User doesn't exist!");
}

module.exports = {
    handleEntries: handleEntries,
    handleApiCall: handleApiCall
}