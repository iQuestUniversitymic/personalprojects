
const checkUser = (email, password, db, bcrypt)=>{

    const user = db('login')
                    .select('email','hash')
                    .where('email', email)
                    .then(async data => {
                            if(data.length){
                                const isValid = bcrypt.compareSync(password, data[0].hash);
                                if (isValid) {
                                    const user = await db('users')
                                        .select('*')
                                        .where('email', email);
                                    return user[0];
                                }
                                return null;
                            }
                            return null;
                        })

    return user;
}

const handleSignIn = async (req, res, db, bcrypt) => {

    const {email, password} = req.body;

    if (!email || !password) {
        return res.status(400).json('Incorrect form submission');
    }

    const user = await checkUser(email, password, db, bcrypt);
    if(user){
        return res.json(user);
    }
    res.status(400).json('Error logging in');

}

module.exports = {
    handleSignIn: handleSignIn
}
