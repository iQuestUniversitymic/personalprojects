
const existsEmail = (email, db) =>{

    return exists = db('users')
                .returning('*')
                .count('*')
                .where('email', email)
                .then(response => {
                    return Boolean(Number(response[0].count))
                });
}

const registerUser = (name, email, password, db, bcrypt) =>{
    
    const hash = bcrypt.hashSync(password);

    const addedUser = db.transaction(trx => {
        
        trx.insert({
            hash: hash,
            email: email
        })
        
        .into('login')
        .returning('email')
        .then(email => 
            
            trx('users')
            .returning('*')
            .insert({
                email: email[0],
                name: name,
                joined: new Date()
            })
            .then(user => {
                return user[0];
            }))
        
        .then(trx.commit)
        .catch(trx.rollback)
    })
    
    return addedUser;
}

const handleRegister = async (req, res, db, bcrypt) => {

    const {name, email, password} = req.body;

    if (!email || !name || !password) {
        return res.status(400).json('Incorrect form submission');
    }
    
    const exists = await existsEmail(email, db);
    if(!exists){
        const addedUser = await registerUser(name, email, password, db, bcrypt);
        return res.json(addedUser);
    }

    res.status(404).json('User already exists!');
}

module.exports = {
    handleRegister: handleRegister
}