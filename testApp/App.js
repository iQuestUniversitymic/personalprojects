import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class App extends React.Component {
  

  constructor(props){
    super(props);
    this.state = {
      data : [],
      apiEndpoint: 'http://192.168.0.157:3000/note'
    }
   }

  componentDidMount(){

    this.getData().then(data => {
      this.setState({
        data: data
      });
      console.log(this.state.data);
    });

  }

  render() {

    return (
      <View style={styles.container}>
        {this.state.data}
      </View>
    );
  }

  
  getData = async () => {

    const response = await fetch(this.state.apiEndpoint);
    const allData = await response.json();
    const notes = await allData['notes'];

    return notes;
  }

}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
