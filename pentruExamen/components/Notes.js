import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput, AsyncStorage, NetInfo} from 'react-native';
import NoteItem from './NoteItem';

export default class Notes extends React.Component {

    constructor(props){
        super(props);
        this.state = {
            noteArray: [],
            offlineNotes: [],
            apiEndpoint: 'http://192.168.0.157:3000/note',
            page: '1',
            offlineDeletedNotes: [],
        }
    }

    componentDidMount(){
    
        console.log("component mount")
        this.getNotesFromServer().then((result) => {
            this.setState({
            noteArray: result
        })}).then(console.log("in component didmount then"))
            .catch("No data my friend!");

        this.synchronizeData();

        let ws = new WebSocket('ws://192.168.0.157:3000');
        ws.onmessage = (e) => {
            
            let event = JSON.parse(e.data);
            
            let data = [...this.state.noteArray];
            data.unshift(event.note);

            this.setState({noteArray : data});
          };

    }

    postOfflineNotes = () => {
        console.log('Sincronizare....');
        NetInfo.isConnected.fetch().then(isConnected => {
        
            if (isConnected && this.state.offlineNotes.length > 0) {
                                
                this.state.offlineNotes.forEach(note => {

                    if(this.state.token){
                        
                        fetch('http://192.168.43.125:44397/notes', {
            
                            method: 'POST',
                            headers: {
                                'Content-Type': 'application/json',
                                'Authorization' : 'Bearer ' + this.state.token
                            },
                            body: JSON.stringify({
                                noteName: note.noteName
                            })
                        })
                            .then((response) => {
                                console.log(this.state.token);
                                response.json();
                                
                                console.log(response);
            
                                if (response.status === 200) {
                                    console.log("S-A ADAUGAT");
                                }
            
                            }).done();
                    }
                })

                this.setState({offlineNotes: []});
            }
        }
    )}

    deleteOfflineNotes = async () => {
        console.log("Sincronizare...");

            this.state.offlineDeletedNotes.forEach(async (deletedNote) => {   
                console.log(deletedNote);
                const isConnected = await NetInfo.isConnected.fetch();

                if(isConnected){
                    
                    console.log(this.state.apiEndpoint +  '/' + deletedNote);
                    const deleteResponse = await fetch(this.state.apiEndpoint +  '/' + deletedNote, {

                    method : 'DELETE',
                    headers: {
                        'Content-Type': 'application/json',
                    },

                    }).catch( () => {
                        console.log("das");
                    });

                    if (deleteResponse) {

                        let actualOfflineDeletedNotes = [...this.state.offlineDeletedNotes];
                        let deletedIndex = actualOfflineDeletedNotes.indexOf(deletedNote);
                        actualOfflineDeletedNotes.splice(deletedIndex, 1);
                        
                        let actualNotes = [...this.state.noteArray];
                        let toDeleteIndex = actualNotes.findIndex(note => note.id === deletedNote);
                        actualNotes.splice(toDeleteIndex, 1);

                        this.setState({offlineDeletedNotes : [...actualOfflineDeletedNotes], noteArray : [...actualNotes]});

                    }
                }
            });
    }

    getNotesFromServer = async () => {
        
        const response = await fetch(this.state.apiEndpoint).catch("Error at retrieving data from server");
        const allData = await response.json();
        let notes = await allData['notes'];
        
        JSON.stringify(notes);
        AsyncStorage.setItem('notes', notes);

        return notes;
    }

    getNotesFromAsyncStorage = async () => {
        return await AsyncStorage.getItem('notes');
    }

    getNextPage = async () => {

       this.state.page = Number(this.state.page) + 1;

        const response = await fetch(this.state.apiEndpoint + "?page=" + this.state.page).catch("Error at new page");
        const allData = await response.json();
        let notes = await allData['notes'];
        
       this.state.noteArray = [...this.state.noteArray, ...notes];
        
        AsyncStorage.setItem('notes', this.state.noteArray);
        this.setState({noteArray : this.state.noteArray, page: this.state.page});

    }

    static navigationOptions = {
        title: 'iNotes',
        headerTitleStyle: {
            fontWeight: 'bold',
            color: 'white'
          },
        headerLeft: null
    };

    synchronizeData = () => {
        //setInterval(this.postOfflineNotes, 20000);
        setInterval(this.deleteOfflineNotes, 20000);        
    }

    render() {

        console.log("render");
        if (!this.state.noteArray.length) {
            console.log("check", this.state.noteArray.length);
            return null;
        }

        let notes = this.state.noteArray.map((note, i) => (
            <NoteItem key={i} val={note.text} deleteMethod={
                () => {if(note.text.localeCompare("----deleted----") !== 0)
                            this.deleteNote(i)}
            }/>
            ))
            

    return (
            <View style={styles.container}>
                <ScrollView style = {styles.scrollContainer} 
                            onScroll = {({nativeEvent}) => {
                                            if (isCloseToBottom(nativeEvent)) {
                                                this.getNextPage();
                                            }
                }}>
                    {notes}
                </ScrollView>

                <View style={styles.footer}>
                    <TextInput 
                        style = {styles.textInput}
                        placeholder = "--> iDol name"
                        placeholderTextColor = "white"
                        underlineColorAndroid = "transparent"
                        value={this.state.noteNameInput}
                        onChangeText = {(text) => this.setState({noteNameInput : text})}>
                    </TextInput>
                </View>

                <TouchableOpacity onPress={this.addNote.bind(this)} style={styles.addButton}>
                    <Text style={styles.addButtonText}>+</Text>
                </TouchableOpacity>
            </View>
            );
    }

    addNote() {

    if (this.state.noteNameInput) {
        
        var noteID = this.state.noteArray[this.state.noteArray.length-1].noteId;
        noteID++;

        var newNote = {
            "noteId" : noteID ,
            "noteName" : this.state.noteNameInput
        };

        this.state.noteArray.push(newNote);

        var noteToSend = this.state.noteNameInput;
        NetInfo.isConnected.fetch().then(isConnected => {
            
           if (isConnected) {
                
                if(this.state.token){
                    
                    fetch('http://192.168.43.125:44397/notes', {
        
                        method: 'POST',
                        headers: {
                            'Content-Type': 'application/json',
                            'Authorization' : 'Bearer ' + this.state.token
                        },
                        body: JSON.stringify({
                            noteName: noteToSend
                        })
                    })
                        .then((response) => {
                            console.log(this.state.token);
                            response.json();
                            
                            console.log(response);
        
                            if (response.status === 200) {
                                console.log("S-A ADAUGAT");
                            }
        
                        }).done();
                }
            }
                else{
                    alert("The Internet connection is off!");
                    this.state.offlineNotes.push(newNote);
                    this.setState({ offlineNotes: this.state.offlineNotes });
                }
        });

        this.setState({ noteArray: this.state.noteArray });
        this.setState({ noteNameInput: ''});    
        console.log(this.state.offlineNotes);

    }

    }

    deleteNote = async (index) => {

    const isConnected = await NetInfo.isConnected.fetch();

    console.log(isConnected);
    let noteArray = [...this.state.noteArray];
    const noteText =  noteArray[index].text;
    const noteId = noteArray[index].id;

    if (isConnected) {

        const deleteResponse = await fetch(this.state.apiEndpoint +  '/' + noteId , {

            method : 'DELETE',
            headers: {
                'Content-Type': 'application/json',
            },

        }).catch( () => {
            alert('The server is not available!');
                    
            let deletedNote = {...noteArray[index]};
            deletedNote.text = "----deleted----";

            noteArray[index] = deletedNote;
            this.state.offlineDeletedNotes.push(noteId);

            this.setState({noteArray : noteArray, offlineDeletedNotes : [...this.state.offlineDeletedNotes]});
        });

        if (deleteResponse) {
            
            alert(`${noteText} has been deleted.`);    
            noteArray.splice(index, 1);
            this.setState({noteArray : noteArray});

            JSON.stringify(noteArray);
            AsyncStorage.setItem('notes', noteArray);
            
            return;
        }

        return;
    }

    alert('Your internet connection is lost!');

    let deletedNote = {...noteArray[index]};
    deletedNote.text = "----deleted----";

    noteArray[index] = deletedNote;
    this.state.offlineDeletedNotes.push(noteId);

    this.setState({noteArray : noteArray, offlineDeletedNotes : [...this.state.offlineDeletedNotes]});
    }

}
 

const styles = StyleSheet.create({

    container: {
        flex: 1
    },
    scrollContainer: {
        flex: 1,
        marginBottom: 90,
    },
    footer: {
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 10,
    },
    textInput: {
        alignSelf: 'stretch',
        color: 'white',
        padding: 20,
        backgroundColor: '#252525',
        borderTopWidth: 2,
        borderTopColor: '#ededed',
    },
    addButton: {
        position: 'absolute',
        zIndex: 20,
        right: 10,
        bottom: 90,
        backgroundColor: '#0d47a1',
        width: 75,
        height: 75,
        borderRadius: 50,
        alignItems: 'center',
        justifyContent: 'center',
        elevation: 8,
    },
    addButtonText: {
        color: "white",
        fontSize: 24
    }
});


const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
    const paddingToBottom = 1;
    return layoutMeasurement.height + contentOffset.y >=
      contentSize.height - paddingToBottom;
  };
