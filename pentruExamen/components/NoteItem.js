import React from 'react';
import { StyleSheet, Text, View, ScrollView, TouchableOpacity, TextInput} from 'react-native';

export default class NoteItem extends React.Component {

    render() {
    return (

        <View style={styles.item}>
            <Text style={styles.itemText}>{this.props.val}</Text>

            <TouchableOpacity onPress={this.props.deleteMethod} style={styles.noteDelete}>
                <Text style={styles.noteDeleteText}>X</Text>
            </TouchableOpacity>
        
        </View>


    );
    }
}
  const styles = StyleSheet.create({

    item: {
        position: 'relative',
        padding: 20,
        paddingRight: 100,
        borderBottomWidth: 2,
        borderBottomColor: '#ededed',
    },

    itemText: {
        paddingLeft: 20,
        borderLeftWidth: 10,
        borderLeftColor: '#7e57c2'
    },

    noteDelete: {
        position: 'absolute',
        justifyContent: 'center',
        alignItems : 'center',
        backgroundColor : '#EE0000',
        padding : 10,
        top : 10,
        bottom : 10,
        right : 10,
    },

    noteDeleteText : {
        color : 'white',
    }
});
