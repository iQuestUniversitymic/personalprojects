import React from 'react';
import { createStackNavigator, createAppContainer } from "react-navigation";
import Notes from "./components/Notes"

const Application = createStackNavigator(
  
    {
      Notes: {screen : Notes},
    },
    {
      defaultNavigationOptions : {
        title : '',
        headerStyle:{
          backgroundColor: '#1a457e'
        }
    }

});

const AppContainer = createAppContainer(Application);

export default class App extends React.Component {
  render() {
    return (
      <AppContainer/>
      );
  }
}