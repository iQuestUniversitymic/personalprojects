import React, {Component} from 'react';
import './App.css';

import CardList from './Components/CardList/CardList';
import AddProba from './Components/AddComponent/AddProba';

class App extends Component {

  addressAPI = "http://192.168.43.245:8080/proba/";

  state = {  
    probeList : [],
  }

  componentDidMount(){
    
    fetch(this.addressAPI + 'all', {
        method: 'get',
        headers: {'Content-type' : 'application/json'}
    })
    .then(res => res.json())
    .then(data => this.setState({probeList: data['probe'] }))
    .catch(error => console.log(error))

  }

  callAPIUpdate = (name, category, newName, newCategory, newEnrolled) => {

    fetch(this.addressAPI + 'update', {
            method: 'post',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                
                name: name,
                category: category,
                
                newName: newName,
                newCategory: newCategory,
                newEnrolled: newEnrolled
            })
        })
        .then(res => console.log('ok'))
        .catch(error => console.log(error))
  
  }

  callAPIAdd = (name, category) => {

    fetch(this.addressAPI + 'add', {
            method: 'post',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                name: name,
                category: category
            })
        })
        .then(res => console.log('ok'))
        .catch(error => console.log(error))
  
  }

  callAPIDelete = (name, category) => {

    fetch(this.addressAPI + 'delete', {
            method: 'post',
            headers: {'Content-type' : 'application/json'},
            body: JSON.stringify({
                name: name,
                category: category
            })
        })
        .then(res => console.log('ok'))
        .catch(error => console.log(error))
  
  }

  updateElement = (name, category, enrolled, newName, newCategory, newEnrolled) =>{
    
    this.callAPIUpdate(name, category, newName, newCategory, newEnrolled);

    const newProbe = {
      name: newName,
      category: newCategory,
      enrolled: newEnrolled
    }

    let newProbeList = this.state.probeList;
    newProbeList[newProbeList.findIndex(probe => probe.name === name && 
                                                  probe.category === category &&
                                                  probe.enrolled === enrolled)] = newProbe;

    this.setState({probeList: newProbeList});
  }
  
  deleteElement = (name, category) =>{

    this.callAPIDelete(name, category);
    
    const newProbeList = this.state.probeList.filter(probe => !(probe.name === name && 
                                                                probe.category === category));

    this.setState({probeList: newProbeList});
  }
  
  addElement = (name, category) =>{

    this.callAPIAdd(name, category);
    
    const newElement = {
      name: name,
      category: category,
      enrolled: 0
    }

    const newProbeList = [...this.state.probeList, newElement];

    this.setState({probeList: newProbeList});
  }
  

  render() { 

    return (  
      <div className='container'>
        <CardList elements={this.state.probeList} 
                  onDeleteElement={this.deleteElement} 
                  onUpdateElement={this.updateElement}/>
        <AddProba onAddElement={this.addElement}/>
      </div>
    );
  
  }

}
 
export default App;