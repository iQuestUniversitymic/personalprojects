import React, { Component } from 'react';
import { Container, Button, Input } from 'semantic-ui-react'; 

class CardParticipant extends Component {

    state = {  
        name: '',
        category: '',
        enrolled: ''
    }

    onNameChange = (event) => {
        this.setState({name: event.target.value})
    }
    
    onCategoryChange = (event) => {
        this.setState({category: event.target.value})        
    }
    
    onEnrolledChange = (event) => {
        this.setState({enrolled: event.target.value})        
    }

    onUpdatePress = () => {
        
        if(this.state.name && 
            this.state.category &&
            this.state.enrolled)

            this.props.onUpdate(this.state.name, this.state.category, this.state.enrolled);
    }
    
    onDeletePress = () => {
        this.props.onDelete();        
    }

    render() { 
        return ( 
            <div className="bg-white dib br3 pa3 ma2 bw2 shadow-5">
                <img src= {"https://robohash.org/" + this.props.name + "?size=200x200"} alt="someRobo"/>
                <div>

                    <h2>{this.props.name}</h2>
                    <span>{this.props.category}</span>&nbsp;&nbsp;<span>{this.props.enrolled} membri</span>
                    <br/><br/>
                    <Container fluid>
        
                        <Input placeholder='Name' onChange={this.onNameChange}/><br/> 
                        <Input placeholder='Category' onChange={this.onCategoryChange}/><br/> 
                        <Input placeholder='Enrolled' onChange={this.onEnrolledChange}/><br/><br/>  

                        <Button basic color='teal' onClick={this.onUpdatePress}>Update</Button>&nbsp;&nbsp;&nbsp;
                        <Button basic color='red' onClick={this.onDeletePress}>Delete</Button>

                    </Container>

                </div>
            </div>
         );
    }
}
 
export default CardParticipant;