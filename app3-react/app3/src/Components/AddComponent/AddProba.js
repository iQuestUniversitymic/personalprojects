import React, { Component } from 'react';
import { Container, Button, Input } from 'semantic-ui-react'; 

class AddProba  extends Component {
    
    state = {  
        name: '',
        category: '',
    }
    
    onNameChange = (event) => {
        this.setState({name: event.target.value})
    }
    
    onCategoryChange = (event) => {
        this.setState({category: event.target.value})        
    }
    
    onAddPress = () => {
        
        if(this.state.name && 
            this.state.category)

            this.props.onAddElement(this.state.name, this.state.category);
    }

    render() { 
        return (  
            <div className="bg-light-blue dib br3 pa3 ma2 bw2 shadow-5 div1">
            <div>

                <h2>Add new proba</h2>
                <Container fluid>
    
                    <Input placeholder='Name' onChange={this.onNameChange}/><br/> 
                    <Input placeholder='Category' onChange={this.onCategoryChange}/><br/> 

                    <Button basic secondary onClick={this.onAddPress}>Add</Button>&nbsp;&nbsp;&nbsp;

                </Container>

            </div>
        </div>  
        );
    }
}
 
export default AddProba;