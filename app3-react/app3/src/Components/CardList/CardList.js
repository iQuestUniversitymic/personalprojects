import React from 'react';

import CardProba from '../CardProba/CardProba';

import './CardList.css';

const CardList = ({elements, onDeleteElement, onUpdateElement}) => {

    return(

        <div className = 'cardsList'>
            {elements.map((probe, index) =>{
                    return <CardProba 
                    key={index} 
                    name={probe.name}
                    category={probe.category}
                    enrolled={probe.enrolled}
                    onDelete={() => onDeleteElement(probe.name, probe.category, probe.enrolled)}
                    onUpdate={(newName, newCategory, newEnrolled) => onUpdateElement(probe.name, probe.category, probe.enrolled, newName, newCategory, newEnrolled)}
                    />
                })}
        </div>

    );

}

export default CardList
