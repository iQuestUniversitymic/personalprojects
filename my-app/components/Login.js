import React from 'react';
import {StyleSheet, Text, View, TextInput, KeyboardAvoidingView, TouchableOpacity, AsyncStorage, Image, ToastAndroid  } from 'react-native';

export default class Login extends React.Component {
  
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: ''
        }
    }
  
componentDidMount(){
    this._loadInitialState().done();
}


static navigationOptions = {
    header : null
};

_loadInitialState = async () => {


    //to remove

    // let token = await AsyncStorage.getItem('token');
    // if (token !== null) {
    //     this.props.navigation.navigate('Artists');
    // }
    AsyncStorage.setItem('artists', null);
}
 

static navigationOptions = {
    header: null
};

    render() {
    return (
        <KeyboardAvoidingView behavior='padding' style={styles.wrapper}>

            <View style={styles.container}>
                <View style={styles.logoContainer}>
                    <Image source={require('../assets/logo1.png')} style ={styles.logo} />
                </View>

                <View style={styles.formControls}>
                    <TextInput style={styles.textInput} placeholder='Username'
                    onChangeText={(username) => this.setState({username}) }
                    underlineColorAndroid='transparent' />

                    <TextInput secureTextEntry={true} style={styles.textInput} placeholder='Password'
                    onChangeText={(password) => this.setState({password}) }
                    underlineColorAndroid='transparent' />

                    <TouchableOpacity
                        style={styles.btn}
                        onPress={this.login}>
                            <Text>Log in</Text>
                        </TouchableOpacity>
                </View>
            </View>
        </KeyboardAvoidingView>
      );
  }

  login = () => {

        if(this.state.username !== '' & this.state.password !== ''){
            fetch('http://192.168.43.125:44397/auth/login', {

                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    username: this.state.username,
                    password: this.state.password
                })
            })
                .then((response) => {
                    response.json();

                    if (response.status === 200) {
                        AsyncStorage.setItem('token', JSON.parse(response._bodyText).token);
                        this.props.navigation.navigate('Artists');
                    }

                }).catch(() => {
                    console.log("Eroare la login!");
                });

                (AsyncStorage.getItem('token').then((value) => {
                fetch('http://192.168.43.125:44397/artists', {

                    method: 'GET',
                    headers: {
                        'Content-Type' : 'application/json',
                        'Authorization' : 'Bearer ' + value
                    }
                })
                    .then((response) => {
                        response.json();
                        if (response.status === 200) {
                            AsyncStorage.setItem('artists',  response._bodyText);
                        }         
                    }).done();

                }
                ));
        }
        else
        {
            ToastAndroid.showWithGravity(
                'The inputs cannot be empty!',
                ToastAndroid.SHORT,
                ToastAndroid.CENTER,
              );
        }  
    }

}

const styles = StyleSheet.create({

    wrapper:{
        flex:1,
        backgroundColor: '#0d47a1'
    },
    container: {
        flex: 1,
        backgroundColor: '#1a237e',
        alignItems: 'center',
        justifyContent: 'center',
        paddingLeft: 40,
        paddingRight: 40,
    },
    header: {
        fontSize: 24,
        marginBottom: 58,
        color: 'white',
        fontWeight: 'bold'
    },
    textInput: {
        alignSelf: 'stretch',
        padding: 16,
        marginBottom: 20,
        backgroundColor: 'white',
        borderRadius: 10
    },
    btn: {
        alignSelf: 'stretch',
        backgroundColor: '#7e57c2',
        padding: 20,
        alignItems: 'center',
        color: 'white',
        borderRadius: 100,
        marginTop: 20,
    },
    logoContainer: {
        alignItems: 'center',
        flexGrow: 1,
        justifyContent: 'center'
    },
    logo: {
        width: 200,
        height: 200
    },
    formControls: {
        flexGrow: 1,
        width: 220 
    }
});
